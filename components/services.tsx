import {
  CodeBracketSquareIcon,
  CommandLineIcon,
  RocketLaunchIcon,
} from "@heroicons/react/20/solid";
import React from "react";

const Services = () => {
  return (
    <div
      id="services"
      className="bg-[#121212] pt-[4rem] md:pt-[8rem] pb-[5rem]"
    >
      <p className="headings">
        My <span className="text-yellow-400">Services</span>
      </p>
      <div
        data-aos="zoom-in-up"
        className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 w-[80%] mx-auto items-center gap-[3rem] mt-[4rem] text-white"
      >
        <div>
          <div className="bg-red-700 hover:scale-110 transform transition-all duration-300 hover:rotate-6 uppercase font-semibold text-center p-[2rem]">
            <CodeBracketSquareIcon className="w-[6rem] h-[6rem] mx-auto text-[#d3fae8]" />
            <h1 className="text-[20px] md:text-[30px] mt-[1.5rem] mb-[1.5rem]">
              Frontend
            </h1>
            <p className="text-[15px] text-[#d3d2d2] font-normal">
              Crafting captivating user interfaces with HTML5, CSS3, and
              JavaScript frameworks like React and Next Js. My pixel-perfect
              designs and responsive solutions ensure seamless experiences
              across all devices.
            </p>
          </div>
        </div>
        <div>
          <div className="bg-orange-700 hover:scale-110 transform transition-all duration-300 uppercase font-semibold text-center p-[2rem]">
            <RocketLaunchIcon className="w-[6rem] h-[6rem] mx-auto text-[#d3fae8]" />
            <h1 className="text-[20px] md:text-[30px] mt-[1.5rem] mb-[1.5rem]">
              Backend
            </h1>
            <p className="text-[15px] text-[#d3d2d2] font-normal">
              Empowering your platform with scalable database architectures, API
              integrations, and robust security measures. From customized
              solutions to performance optimization, I lay the foundation for
              sustainable growth.
            </p>
          </div>
        </div>
        <div>
          <div className="bg-blue-700 hover:scale-110 transform transition-all duration-300 hover:rotate-6 uppercase font-semibold text-center p-[2rem]">
            <CommandLineIcon className="w-[6rem] h-[6rem] mx-auto text-[#d3fae8]" />
            <h1 className="text-[20px] md:text-[30px] mt-[1.5rem] mb-[1.5rem]">
              Fullstack
            </h1>
            <p className="text-[15px] text-[#d3d2d2] font-normal">
              Bridging frontend elegance with backend power, my full stack
              expertise delivers end-to-end solutions. With agile methodologies
              and continuous optimization, I architect holistic experiences that
              drive tangible business outcomes.zons and take on new technologies
              as the need arises.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Services;
