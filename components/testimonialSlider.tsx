import React from 'react'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import ClientReview from './clientReview';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

const TestimonialSlider = () => {
  return (
    <Carousel
    additionalTransfrom={0}
    arrows={true}
    autoPlay={true}
    autoPlaySpeed={5000}
    centerMode={false}
    infinite
    responsive={responsive}
    itemClass='item'
  >
    <ClientReview image='/images/mc7.jpg' source='Linkedin' name='Hassan Mateen' role='Software Engineer' comment='Very enthusiastic, passionate, talented and honest about work and work ethics.' />
    <ClientReview image='/images/mc8.jpg' source='Linkedin' name='Muhammad Shakaib' role='Software Engineer' comment='I recomend Najeeullah for his excellent problem solving skills, logic building, dedication, skills and motivation. The way he works under pressure is enormous. Highly recommended Software Engineer !' />
    <ClientReview image='/images/mc1.jpg' source='Linkedin' name='Ahsan Soomro' role='Software Engineer' comment='I had the pleasure of working alongside NajeeUllah and can confidently say that he is an exceptional developer. His expertise, dedication, and collaborative spirit significantly contributed to the success of our projects. I would highly recommend NajeeUllah for any development role and am confident in his ability to excel in any team or project.' />
    <ClientReview image='/images/mc2.jpg' source='Linkedin' name='Zubair Bin Khalid' role='Software Engineer' comment='A highly passionate, intelligent and hard working co-worker with skill to perform well under pressure and one who comes up with an efficient solution to the problem.' />
    <ClientReview image='/images/mc3.jpg' source='Linkedin' name='Finza Mughal' role='Software Project Manager' comment='Najee is a very responsible resource and completely owns his assigned tasks. He is good at logic building and his analytical skills helped us scale our various products.' />
    <ClientReview image='/images/mc4.jpg' source='Linkedin' name='Muhammad Shaheer Khan' role='Software Engineer' comment='I highly recommend Najee for his exceptional dedication and expertise in software development. Its been a pleasure working alongside someone with such professionalism and commitment.' />
    <ClientReview image='/images/mc5.jpg' source='Linkedin' name='Muhammad Irtaza Qureshi' role='Software Engineer' comment='Hard working and technical personnel.' />
    <ClientReview image='/images/mc6.jpg' source='Fiver' name='hilaldeveloper' role='Fiver Client' comment='Nice seller to work with. Delivered the work on time.' />
  </Carousel>
  )
}

export default TestimonialSlider
