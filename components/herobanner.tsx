import React from "react";
import TextEffects from "./textEffects";
import Image from "next/image";
import { ArrowDownTrayIcon, LinkIcon } from "@heroicons/react/20/solid";
import Particle from "./particle";

const HeroBanner = () => {
  return (
    <div
      id="home"
      className="h-[88vh] bg-[url('/images/banner.jpg')] mt-[10vh] bg-cover bg-center"
    >
      <Particle />
      <div className="w-[80%] grid-cols-1 mx-auto grid lg:grid-cols-2 gap-[3rem] h-[100%] items-center">
        <div>
          <h1 className="text-[35px] md:text-[50px] text-white font-bold">
            HI, I AM{" "}
            <span className="text-yellow-400"> NAJEEULLAH YOUSFANI!</span>{" "}
          </h1>
          <TextEffects />
          <p className="mt-[1.5rem] text-[20px] text-[#ffffff92]">
            A Fullstack Software Engineer, proficient in both LAMP and MERN
            stacks.
          </p>

          <div className="mt-[1.5rem] flex-col space-y-6 sm:space-y-0 sm:flex sm:flex-row items-center sm:space-x-6">
            <a
              href="/cv/Najeeullah_CV.pdf"
              download="Najeeullah_CV.pdf"
              className="px-[2rem] hover:bg-yellow-400 transition-all duration-200 py-[1rem] text-[18px] font-bold uppercase bg-[#55e6a5] text-black flex items-center space-x-2"
            >
              <p>Download CV</p>
              <ArrowDownTrayIcon className="w-[1.6rem] h-[1.7rem] text-black" />
            </a>

            <a
              href="https://gitlab.com/users/yousfaninajeeullah/projects"
              target="_blank"
              className="px-[2rem] hover:bg-yellow-400 transition-all duration-200 py-[1rem] text-[18px]
             font-bold uppercase bg-[#55e6a5] text-black flex items-center space-x-2"
            >
              <LinkIcon className="w-[1.6rem] h-[1.7rem] text-black" />
              <p>Git Repositories</p>
            </a>
          </div>
        </div>
        <div className="w-[600px] hidden bg-black-400 relative lg:flex items-center rounded-full h-[600px]">
          <Image
            src="/images/u4.jpg"
            alt="user"
            layout="fill"
            className="object-cover rounded-full"
          />
        </div>
      </div>
    </div>
  );
};

export default HeroBanner;
