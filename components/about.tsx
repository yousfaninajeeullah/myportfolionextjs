import { LinkIcon } from "@heroicons/react/20/solid";
import Image from "next/image";
import React from "react";

const About = () => {
  return (
    <div id="about" className="bg-[#121121] pb-[3rem] pt-[4rem] md:pt-[8rem]">
      <div className="grid grid-cols-1 md:grid-cols-2 w-[80%] mx-auto gap-[3rem] items-center">
        <div>
          <h1 className="text-[20px] font-bold uppercase text-[#55e6a5] mb-[1rem]">
            ABOUT ME
          </h1>
          <h2 className="text-[25px] md:text-[35px] lg:text-[45px] md:leading-[3rem] leading-[2rem] capitalize mb-[3rem] font-bold text-white">
            Tranforming <span className="text-yellow-400">Vision</span>
          </h2>
          <div className="mb-[3rem] flex items-center md:space-x-10">
            <span className="w-[100px] hidden md:block h-[5px] bg-slate-400 rounded-sm"></span>
            <p className="text-[19px] text-slate-300 text-justify w-[100%]">
              I&apos;m a web developer with almost 3 years of experience in both
              back-end and front-end development. I excel at crafting dynamic,
              high-performance, and scalable web applications that precisely
              cater to client requirements. My skill set encompasses API
              development, seamless integration of third-party APIs, robust
              database system design and implementation, and crafting custom
              solutions for intricate challenges. As a full-stack software
              engineer, I&apos;m proficient in both LAMP and MERN stacks.
              I&apos;m also eager to broaden my horizons and take on new
              technologies as the need arises.
            </p>
          </div>
          <a
            href="https://www.linkedin.com/in/najeeullah/"
            target="_blank"
            className="px-[2rem] w-[100%] hover:bg-yellow-400 transition-all duration-200 py-[1rem] text-[18px]
             font-bold uppercase bg-[#55e6a5] text-black flex items-center space-x-2"
          >
            <LinkIcon className="w-[1.6rem] h-[1.7rem] text-black" />
            <p>Linkedin</p>
          </a>
        </div>
        <div
          data-aos="fade-left"
          className="lg:w-[500px] mx-auto md:mx-0  mt-[2rem] lg:mt-0 lg:h-[500px] w-[300px] h-[300px] relative"
        >
          <Image
            src="/images/about.jpg"
            alt="user"
            layout="fill"
            objectFit="contain"
            className="relative z-[11] w-[100%] h-[100%] object-contain"
          />
          <div className="absolute w-[100%] h-[100%] z-[10] bg-[#55e6a5] top-[-2rem] right-[-2rem]"></div>
        </div>
      </div>
    </div>
  );
};

export default About;
