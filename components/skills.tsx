import React from "react";
import SkillsItem from "./skillsItem";
import SkillsLanguage from "./skillsLanguage";

const Skills = () => {
  return (
    <div
      id="experience"
      className="pt-[4rem] md:pt-[8rem] pb-[5rem] bg-[#09101a]"
    >
      <h1 className="headings">
        Experiences & <span className="text-yellow-400">Skills</span>
      </h1>
      <div className="w-[80%] mx-auto pt-[4rem] md:pt-[8rem] grid grid-cols-1 md:grid-cols-2 gap-[2rem] items-center">
        <div data-aos="fade-right">
          <SkillsItem
            title="Full Stack Developer"
            year="2023 - Present"
            description=" Development of API's, CRM and Web applications Lamp Stack (Laravel PHP, Mysql). 
            Worked on a project from scratch Mern Stack (Reactjs, Nestjs, postgreSQL) Test Driven Development (TDD). 
            Designing ERD's and Databases. 
            Resolving bug tickets. 
            Daily Scrum Meetings and client calls"
          />
          <SkillsItem
            title="Associate Software Engineer"
            year="2021 - 2022"
            description="Development of API's on Laravel and Node js. Documentation of API's on Postman. Designing ERD's and Databases. Unit testing. Guiding juniors, Daily and weekly project planing meetings and client calls"
          />
          <SkillsItem
            title="Trainee Wordpress Developer"
            year="2020 - 2021"
            description="Basic customization of Web, Theme and plugins. Resolving Client Reported Issues. Wordpress site migrations"
          />
          <SkillsLanguage
            skill1="mysql"
            skill2="php"
            skill3="laravel"
            skill4="lumen"
            skill5="Git"
            skill6="postman"
            skill7="Rest api"
            level1="w-[80%]"
            level2="w-[70%]"
            level3="w-[90%]"
            level4="w-[100%]"
            level5="w-[100%]"
            level6="w-[100%]"
            level7="w-[95%]"
          />
        </div>
        <div data-aos="fade-left">
          <SkillsItem
            title="Associate Software Engineer"
            year="2022 - 2023"
            description=" Development of API's, CRM and Web applications Lamp Stack (Laravel PHP, Mysql). 
            Worked on a project from scratch Mern Stack (Reactjs, Nestjs, postgreSQL) Test Driven Development (TDD). 
            Designing ERD's and Databases. 
            Resolving bug tickets. 
            Daily Scrum Meetings and client calls"
          />
          <SkillsItem
            title="Intern Software Engineer"
            year="2021 - 2021"
            description="Leaning of API Development, Testing, Documentation and maintenance. Learning of Laravel PHP and Node js. Project plannings and client calls"
          />
          <SkillsItem
            title="Intern Web Developer"
            year="2020 - 2020"
            description="Developed web site keydee.com.pk. Developed and integrated C# Desktop App with kaydee.com.pk"
          />
          <SkillsLanguage
            skill1="pgsql"
            skill2="Javascript"
            skill3="React Js"
            skill4="Node Js"
            skill5="Next Js"
            skill6="Swagger"
            skill7="Nest Js"
            level1="w-[70%]"
            level2="w-[70%]"
            level3="w-[80%]"
            level4="w-[60%]"
            level5="w-[90%]"
            level6="w-[100%]"
            level7="w-[95%]"
          />
        </div>
      </div>
    </div>
  );
};

export default Skills;
