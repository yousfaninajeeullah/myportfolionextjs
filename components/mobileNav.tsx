import { XMarkIcon } from "@heroicons/react/20/solid";
import Link from "next/link";
import React from "react";

interface Props {
    nav:  boolean;
    closeNav: () => void;
  }

const MobileNav = ({nav, closeNav}:Props) => {
    const navAnimation = nav ? 'translate-x-0' : 'translate-x-[-100%]';
  return (
    <div
      className={`fixed ${navAnimation} transform transition-all duration-300 top-0 left-0 right-0 bottom-0 z-[100000000] bg-[#09101a]`}
    >
      <div className="w-[100vw] h-[100vh] flex flex-col items-center justify-center">
        <div className="nav-links-mobile"><Link scroll={false} href='#home'>HOME</Link></div>
        <div className="nav-links-mobile"><Link scroll={false} href='#about'>ABOUT</Link></div>
        <div className="nav-links-mobile"><Link scroll={false} href='#services'>SERVICES</Link></div>
        <div className="nav-links-mobile"><Link scroll={false} href='#education'>EDUCATION</Link></div>
        <div className="nav-links-mobile"><Link scroll={false} href='#experience'>EXPERIENCE</Link></div>
        <div className="nav-links-mobile"><Link scroll={false} href='#recomendation'>RECOMENDATIONS</Link></div>
        <div className="nav-links-mobile"><Link scroll={false} href='#contact'>CONTACT</Link></div>
      </div>

      <div onClick={closeNav}  className="absolute z-[100000000] cursor-pointer top-[2rem] right-[2rem] h-[2rem] w-[2rem] text-yellow-300">
        <XMarkIcon/>
      </div>
    </div>
  );
};

export default MobileNav;
