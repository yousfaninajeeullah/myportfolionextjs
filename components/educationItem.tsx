import Image from "next/image";
import React from "react";

interface Props {
  title: string;
  year: string;
  logo: string;
  details: string;
}

const EducationItem = ({ title, year, logo, details }: Props) => {
  return (
    <div className="mb-[4rem] md:mb-[8rem]">
      {logo ? (
        <div data-aos="flip-right">
          <div className="cursor-pointer relative w-[100%] mt-[1rem] h-[100px] md:h-[200px] mb-[0rem]">
            <Image
              src={`/images/${logo}.png`}
              alt="portfolio"
              layout="fill"
              className="object-contain"
            />
          </div>
        </div>
      ) : (
        ""
      )}

      <h1 className="mt-[2rem] font-semibold mb-[1rem] text-[15px] text-center sm:text-[25px] md:text-[34px] text-white uppercase">
        {title}
      </h1>
      <p className="text-[#aaaaaa] text-center font-normal text-[17px] opacity-80">
        {details}
      </p>
      <p className="px-[2rem] flex justify-center items-center text-[#55e6a5] py-[0.9rem] font-bold text-[18px] ">
        {year}
      </p>
    </div>
  );
};

export default EducationItem;
