import { TypeAnimation } from 'react-type-animation';

const TextEffects = () => {
  return (
    <TypeAnimation
      sequence={[
        // Same substring at the start will only be typed out once, initially
        'Full Stack Developer',
        1500, // wait 1s before replacing "Mice" with "Hamsters"
        'Backend Developer',
        1500,
        'Frontend Developer',
        1500,
        'Software Engineer',
        1500
      ]}
      speed={50}
      className='text-[2rem] md:text-[3rem] text-[#55e6a5] font-bold uppercase'
      repeat={Infinity}
    />
  );
};

export default TextEffects;