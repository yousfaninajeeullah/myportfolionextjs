import React from "react";
import EducationItem from "./educationItem";

const Education = () => {
  return (
    <div id='education' className="pt-[4rem] md:pt-[8rem] pb-[5rem] bg-[#09101a]">
      <h1 className="headings">
        Educ<span className="text-yellow-400">ation</span>
      </h1>
      <div className="w-[80%] mx-auto pt-[4rem] md:pt-[8rem] grid grid-cols-1 md:grid-cols-2 gap-[2rem] items-center">
        <div data-aos="fade-right">
          <EducationItem logo="bahria" title="Bachelors of Science in Computer Science (BSCS)" year="2017 - 2021" details="Bahria University, Karachi Campus" />
        </div>
        <div data-aos="fade-left">
          <EducationItem logo="rgb" title="Master of Science in Computer Science (MSCS)" year="2023 - Present"  details="Universität Siegen, Siegen"/>
        </div>
      </div>
    </div>
  );
};

export default Education;
