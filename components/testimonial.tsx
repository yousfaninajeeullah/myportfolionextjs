import React from 'react'
import TestimonialSlider from './testimonialSlider'

const Testimonial = () => {
  return (
    <div id='recomendation' className='bg-[#02050a] md:pt-[8rem] pd-[1rem]'>
      <h1 className='headings'>
        Client <span className='text-yellow-400'>Reviews & Recommend</span>ations 
      </h1>
      <div className='pt-[5rem] pb-[4rem] w-[80%] mx-auto'>
        <TestimonialSlider/>
      </div>
    </div>
  )
}

export default Testimonial
