import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html className="scroll-smooth" lang="en">
      <Head />
      <body className="font-primary">
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
